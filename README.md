# Welcome to Frontend Challenge

In this challange you will have to develop a blog page using best practices of

frontend programming language such as `HTML`, `CSS` and `JavaScript`.

The blog page will have to look exactly like the example bellow.

![](blog-images/blog-page.png)

# What we want from you

*  Clean and commented code
*  Right indented code
*  Organized files
*  Semantic `HTML` tags

# Instructions 

*  Clone this repository to get the images.
*  Create your own git repository.
*  Upload your project and send an email with the link of it to `frontend@gruponeolife` with copy to `alan@gruponeolife.com.br`.

>  Good look and happy coding!!
